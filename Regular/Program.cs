﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;

namespace Regular
{
    class Program
    {
        static void Main(string[] args)
        {
            string targetUrl = "https://www.tk-nav.ru";
            var htmlString = getHtmlContent(targetUrl).ConfigureAwait(false).GetAwaiter().GetResult();

            if(String.IsNullOrEmpty(htmlString))
            {
                Console.WriteLine("Не удалось загрузить страницу");
                return;
            }

            var picturesUrls = findPicturesUrl(htmlString, targetUrl);

            if (picturesUrls.Count == 0)
            {
                Console.WriteLine("Не удалось найти изображения на странице");
            }

            Console.WriteLine($"Найдено {picturesUrls.Count} изображений");

            for (int i = 0; i < picturesUrls.Count; i++)
            {
                var picture = getHtmlContentAsBytes(picturesUrls[i]).ConfigureAwait(false).GetAwaiter().GetResult();

                if (picture == null)
                {
                    Console.WriteLine($"Не удалось загрузить изображение: {picturesUrls[i]}");
                    continue;
                }
                else
                {
                    var name = (i + 1) + "-" + getNameFromUrl(picturesUrls[i]);
                    File.WriteAllBytes(name, picture);
                    Console.WriteLine("Сохранен файл: " + name);
                }
            }
        }



        private static string getNameFromUrl(string url)
        {
            string pattern = @"((\w+)$)|((\w+\.\w+)$)";

            MatchCollection matches = Regex.Matches(url, pattern);

            if(matches.Count > 0)
            {
                return (matches[0].Value.Length < 256) ? matches[0].Value : null;
            }

            return null;
        }



        static async Task<string> getHtmlContent(string url)
        {
            HttpClient httpClient = new HttpClient();

            var httpAnswer = await httpClient.GetAsync(url);
            if(!httpAnswer.IsSuccessStatusCode)
            {
                return null;
            }
            var body = await httpAnswer.Content.ReadAsStringAsync();

            return body;
        }



        static async Task<byte[]> getHtmlContentAsBytes(string url)
        {
            HttpClient httpClient = new HttpClient();

            var httpAnswer = await httpClient.GetAsync(url);
            if (!httpAnswer.IsSuccessStatusCode)
            {
                return null;
            }
            var body = await httpAnswer.Content.ReadAsByteArrayAsync();

            return body;
        }



        static List<string> findPicturesUrl(string htmlString, string baseUrl)
        {
            List<string> retStrings = new List<string>();

            string pattern = @"((\<img\s+src=""(?<Full>http(s?).+?)"")|(\<img\s+src=""(?<Short>.+?)""))";

            MatchCollection matches = Regex.Matches(htmlString, pattern);

            foreach (Match match in matches)
            {
                var getedUrl = match.Groups["Full"].Value;
                if (String.IsNullOrEmpty(getedUrl))
                {
                    getedUrl = match.Groups["Short"].Value;
                    if (!String.IsNullOrEmpty(getedUrl))
                    {
                        getedUrl = baseUrl + getedUrl;
                    }
                }
                retStrings.Add(getedUrl);
            }

            return retStrings;
        }
    }
}
